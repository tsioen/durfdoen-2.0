---
titel: Vlaamse Psychologische en Pedagogische Kring
id: vppk
naam: Vlaamse Psychologische en Pedagogische Kring
verkorte_naam: Vlaamse Psychologische en Pedagogische Kring
konvent: fk
website: https://www.vppk.be
contact: praeses@vppk.be
themas:
  -  faculteit
---
$lang=nl$ 
Als VPPK – studentenvereniging van de Faculteit Psychologie en Pedagogische Wetenschappen (UGent) – engageren wij ons om een waardevolle bijdrage te leveren aan het studentenleven. Zo voorzien wij jaarlijks duizenden studenten van hun boeken en cursussen, organiseren wij talloze soorten activiteiten en zorgen wij ervoor dat studenten over heel Gent kunnen genieten van allerlei voordelen. 
$langend$ 
$lang=en$ 
As VPPK - student association for Ghent University  Psychology and Educational Sciences students - we are committed to contribute in a meaningful way to the lives of students. On a yearly basis we provide thousands of students with their textbooks, organize numerous activities and make sure that students can enjoy benefits across Ghent. 
$langend$
