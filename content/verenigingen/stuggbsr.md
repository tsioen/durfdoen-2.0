---
titel: Biomedische Studentenraad
id: stuggbsr
naam: Biomedische Studentenraad
verkorte_naam: Biomedische Studentenraad
konvent: gsr
website:
contact: bsr@ugent.be 
social:
  - platform: facebook
    link: https://www.facebook.com/BiomedischeStudentenraad
themas:
  - engagement 
showcase:
  - photo: /assets/logos/BSRA.jpg
  - photo: /assets/logos/BSRB.jpg
  - photo: /assets/logos/BSRC.jpg
---

De Biomedische Studentenraad, is een vereniging van vrijwilligers die zich vol enthousiasme inzet voor de opleiding. De BSR is verantwoordelijk voor de doorstroom van informatie zodat waardevolle feedback op de juiste plaats terecht komt. We verzorgen ook een aantal initiatieven die bijdragen tot een ruimere kadering van de opleiding biomedische wetenschappen. Zo organiseren we onder meer evenementen als de Careerday, Infodates, Meet & Greet (voor 1ste bachelor studenten) en een Filmavond. Voor zijn leden is BSR ook een skillslab dat de mogelijkheid biedt tot het ontplooien van vaardigheden op vlak van teamwork, communicatie en organisatie!
