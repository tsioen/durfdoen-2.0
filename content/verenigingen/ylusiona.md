---
titel: Ylusiona
id: ylusiona
naam: Ylusiona
verkorte_naam: Ylusiona
konvent: kultk
contact: ylusiona@student.ugent.be
themas:
  -  lifestyle
---
Vereniging van mindfucks, magie en illusies.
