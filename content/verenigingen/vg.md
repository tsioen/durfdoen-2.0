---
titel: Verkeerd geparkeerd
id: vg
naam: Verkeerd geparkeerd
verkorte_naam: Verkeerd geparkeerd
konvent: wvk
contact: info@verkeerdgeparkeerd.be
website: http://www.verkeerdgeparkeerd.be/
social:
  - platform: facebook
    link: https://www.facebook.com/verkeerdgeparkeerd/
themes:
  -  diversiteit
  -  lifestyle
showcase:
  - photo: /assets/logos/VerkeerdGeparkeerdA.jpg
  - photo: /assets/logos/VerkeerdGeparkeerdB.jpg
  - photo: /assets/logos/VerkeerdGeparkeerdC.jpg
---

$lang=nl$ 
Lesbian, gay, bisexual, trans, queer of nog iets anders? Dan is VG iets voor jou! Wij zijn een LGBTQ+ jongerenvereniging uit Gent en organiseren wekelijkse café-avonden, activiteiten en fuiven. Bovendien sensibiliseren we andere jongeren omtrent het LGBTQ thema door verschillende acties en onze scholenwerking. Zin om nieuwe vrienden te maken, het Gentse LGBTQ leven te ontdekken of gewoon eens bij ons een drankje te drinken?
Kom dan zeker eens af! 
$langend$ 
$lang=en$ 
Lesbian, gay, bisexual, trans, queer or someting else? Then VG is here for you. We are a LGBTQ+ youth association out of Ghent and organize weekly bar nights, activities and parties. Moreover, we raise awareness with other young people about LGBTQ related topics by way of different actions and educational programmes. Do you want to make new friends, discover the LGBTQ life in Ghent or just enjoy a drink with us? Feel free to drop by anytime. 
$langend$
