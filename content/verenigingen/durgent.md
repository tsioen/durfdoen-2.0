---
naam: D'URGENT
id: durgent
verkorte_naam: D'URGENT
titel: D'URGENT
konvent: wvk
website: https://www.greencommunity.be/durgent
social:
  - platform: youtube
  - platform: facebook
    link: https://www.facebook.com/durgentduurzaamdoorstudenten/
  - platform: instagram
    link: https://www.instagram.com/_durgent/
themas:
  - groen
showcase:
  - photo: /assets/logos/durgentA.jpg
  - photo: /assets/logos/durgentB.jpg
  - photo: /assets/logos/durgentC.jpg
---

$lang=nl$ 
D'URGENT is een studentenbeweging die werkt rond duurzaamheid. We vertrekken vanuit het idee dat studenten een grote impact kunnen hebben op hun leefomgeving.
Wij staan open voor eenieder die goesting heeft om de zaken aan te pakken, elk op zijn eigen interessegebied, elk op zijn eigen tempo. Hoe veel tijd en werk je in de organisatie steekt is volledig je eigen keuze, en ongeacht welke keuze je maakt, je steentje zal je zeker kunnen bijdragen! 
$langend$ 
$lang=en$ 
D’URGENT is a student movement dedicated to sustainability. We depart from the idea that students can have a big impact on their living environment. We are open to everyone who feels like tackling the important issues, each one in their own area of interest and at their own pace. How much time you invest in the organisation is totally up to you and no matter what your choice is, you will definitely be able to contribute! 
$langend$
